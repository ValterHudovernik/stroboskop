# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://ValterHudovernik@bitbucket.org/ValterHudovernik/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/ValterHudovernik/stroboskop/commits/65546df72a04c0c3c0bc4ef4e20762694d1145bc

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/ValterHudovernik/stroboskop/commits/c679346f86bc41e2f3d7e48bb215261be79907fb?at=izgled

Naloga 6.3.2:
https://bitbucket.org/ValterHudovernik/stroboskop/commits/541f7fa8d8a8ff10958cdaa3d9414b15026a788b

Naloga 6.3.3:
https://bitbucket.org/ValterHudovernik/stroboskop/commits/37f239e838ab7d454baf392c76a24d9b36ea360b?at=izgled

Naloga 6.3.4:
https://bitbucket.org/ValterHudovernik/stroboskop/commits/b82dda93bf36059d30a9dcd8639d32ec0ec1e362

Naloga 6.3.5:

```
    git checkout master
    git merge izgled
    git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/ValterHudovernik/stroboskop/commits/049835861f2597a567fb0de4786dba91e2cd7aff?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/ValterHudovernik/stroboskop/commits/7a9a342da900203c021d14198da524a2564d1efb

Naloga 6.4.3:
https://bitbucket.org/ValterHudovernik/stroboskop/commits/0b5214c36e439ea82eeb9b8e4888225ccee451b3

Naloga 6.4.4:
https://bitbucket.org/ValterHudovernik/stroboskop/commits/d4404f9a7ab52b9ebd58ec9c29d405c099813a88